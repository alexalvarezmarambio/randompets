import { Component, OnInit } from '@angular/core';
import { DogService } from 'src/app/dog.service';

@Component({
  selector: 'app-razas',
  templateUrl: './razas.component.html',
  styles: [
  ]
})
export class RazasComponent implements OnInit {

  constructor(public dog: DogService) { }

  ngOnInit(): void {

    this.dog.todasRazas();

  }

  meGusta(index: number) {

    this.dog.razas[index].megusta = !this.dog.razas[index].megusta;
  }

}
