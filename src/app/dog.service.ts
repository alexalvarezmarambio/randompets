import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DogService {

  api = 'https://dog.ceo/api';
  razas = [];

  constructor(private http: HttpClient) { }

  todasRazas() {

    this.http.get(`${ this.api }/breeds/list`).subscribe( (response: any) => {
      this.imagenRaza(response.message);
    });

  }

  imagenRaza(razasArreglo: string[]) {

    razasArreglo.forEach( element => {

      this.http.get(`${ this.api }/breed/${ element }/images/random`).subscribe( (response: any) => {
        const aux = {
          nombre: element,
          imagen: response.message,
          megusta: false
        };

        this.razas.push(aux);
      });

    });

  }
}
